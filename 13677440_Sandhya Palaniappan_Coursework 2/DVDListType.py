

import csv
from DVD import DVD


# class Node
class Node:
    def __init__(self, dvd=None, next=None):
        self.dvd = dvd
        self.next = next


# class DVDListType using Linked List
class DVDListType:
    def __init__(self):
        self.head = None
        
    def add_dvd(self, dvd):
        new_node = Node(dvd)
        if self.head is None:
            self.head = new_node
            return
        current = self.head
        while current.next is not None:
            current = current.next
        current.next = new_node
        
    def remove_dvd(self, name):
        if self.head is None:
            return
        if self.head.dvd.name == name:
            self.head = self.head.next
            return
        current = self.head
        while current.next is not None:
            if current.next.dvd.movie_name == name:
                current.next = current.next.next
                return
            current = current.next
    
    def search_dvd(self, name):
        current = self.head
        while current is not None:
            if current.dvd.movie_name == name:
                return current.dvd
            current = current.next
        return None
    
    def print_list(self):
        current_node = self.head
        while current_node is not None:
            print(current_node.dvd)
            current_node = current_node.next

    
    def save_to_file(self):
        with open("dvds_list.csv", 'w', newline='') as csvfile:
            fieldnames = ['Name', 'Stars', 'Producer', 'Director', 'Production Company', 'Copies', 'Rental Fee']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            current = self.head
            while current is not None:
                writer.writerow({'Name': current.dvd.movie_name, 'Stars': current.dvd.stars, 'Producer': current.dvd.producer,
                                 'Director': current.dvd.director, 'Production Company': current.dvd.production_company,
                                 'Copies': current.dvd.copies, 'Rental Fee': current.dvd.rental_fee})
                current = current.next
    
    def load_from_file(self):
        with open("dvds_list.csv", 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                dvd = DVD(row['Name'], row['Stars'], row['Producer'], row['Director'], row['Production Company'],
                          row['Copies'], row['Rental Fee'])
                self.add_dvd(dvd)


    def print_dvd_names(self):
        current = self.head
        while current is not None:
            print(current.dvd.movie_name)
            current = current.next
  

    def print_dvd_info(self, name):
        dvd = self.search_dvd(name)
        if dvd:
            print(f"Name: {dvd.movie_name}")
            print(f"Stars: {dvd.stars}")
            print(f"Producer: {dvd.producer}")
            print(f"Director: {dvd.director}")
            print(f"Production Company: {dvd.production_company}")
            print(f"Copies: {dvd.copies}")
            print(f"Rental Fee: {dvd.rental_fee}")
        else:
            print(f"No DVD with name {name} found.")

dvd_list = DVDListType()
dvd_list.load_from_file()

dvd_list.print_dvd_info("Aladdin")
dvd_list.print_dvd_info("Mulan")
dvd_list.print_dvd_info("The Greatest Showman")
dvd_list.print_dvd_info("Shang-Chi and the Legend of the Ten Rings")
dvd_list.print_dvd_info("Spider-Man: No Way Home")
dvd_list.print_dvd_info("Mrs. Doubtfire")
dvd_list.print_dvd_info("The Pacifier")