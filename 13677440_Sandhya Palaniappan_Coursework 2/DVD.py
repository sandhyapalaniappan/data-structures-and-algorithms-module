

# class DVD
class DVD:
    def __init__(self, movie_name, stars, producer, director, production_company, copies, rental_fee):
        self.movie_name = movie_name
        self.stars = stars
        self.producer = producer
        self.director = director
        self.production_company = production_company
        self.copies = copies
        self.rental_fee = rental_fee
        
    def __str__(self):
        return f"{self.movie_name}, {self.stars}, {self.producer}, {self.director}, {self.production_company}, {self.copies}, {self.rental_fee}" 