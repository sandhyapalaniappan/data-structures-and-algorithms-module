

import csv
from datetime import datetime
from CustomerBTreeType import CustomerBTreeType
from DVD import DVD



# class DVDStore
class DVDStore:
    def __init__(self, file_path):
        self.dvds = []
        self.file_path = file_path
        self.head = None
        self.customer_list = []
        self.customer_tree = CustomerBTreeType('customer_list.csv')
        self.dvds = self.load_dvd_list()
        self.build_list()



    def load_dvd_list(self):
        dvds = []
        with open(self.file_path, 'r') as file:
            reader = csv.reader(file)
            headers = next(reader) 
            for row in reader:
                name, stars, producer, director, production_company, copies, rental_fee = row
                dvd = [name, stars, producer, director, production_company, copies, rental_fee]
                self.dvds.append(dvd)
        return dvds


    def load_customer_list(self):
        with open("customer_list.csv", 'r') as file:
            reader = csv.reader(file)
            for row in reader:
                self.customer_list.append(row)
        self.customer_tree.initialize_customer_tree(self.customer_list)



    # Create a list of DVDs Owned by the Store.
    def add_dvd(self):
        new_dvd = []
        new_dvd.append(input("Please Enter the Name of the Movie:"))
        new_dvd.append(input("Please Enter the Name of the Stars:"))
        new_dvd.append(input("Please Enter the Name of the Producer:"))
        new_dvd.append(input("Please Enter the Name of the Director:"))
        new_dvd.append(input("Please Enter the Name of the Production Company:"))
        new_dvd.append(input("Please Enter the Number of Copies in the Store:"))
        new_dvd.append(input("Please Enter the Rental Fee of the DVD:"))
        with open(self.file_path, 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(new_dvd)
        self.dvds.append(new_dvd)
        print("DVD added successfully!")



    # Edit DVD Information.
    def edit_dvd(self):
        name = input("Please Enter the Name of the DVD that you would like to Edit:")
        with open(self.file_path, "r") as file:
            reader = csv.reader(file)
            rows = list(reader)
        for i, row in enumerate(rows):
            if row[0] == name:
                print("Please Enter the Edited DVDs Information:")
                row[0] = input("Please Enter the Name of the Movie:")
                row[1] = input("Please Enter the Name of the Stars:")
                row[2] = input("Please Enter the Name of the Producer:")
                row[3] = input("Please Enter the Name of the Director:")
                row[4] = input("Please Enter the Name of the Production Company:")
                row[5] = input("Please Enter the Number of Copies in the Store:")
                row[6] = input("Please Enter the Rental Fee of the DVD:")
                with open(self.file_path, "w", newline='') as file:
                    writer = csv.writer(file)
                    writer.writerows(rows)
                for dvd in self.dvds:
                    if dvd.movie_name == name:
                        dvd.movie_name = row[0]
                        dvd.stars = row[1]
                        dvd.producer = row[2]
                        dvd.director = row[3]
                        dvd.production_company = row[4]
                        dvd.copies = row[5]
                        dvd.rental_fee = row[6]
                        break
                print("DVD Information has been Edited Successfully!")
                return
        print(f"DVD '{name}' is Not Found.")



    # Update DVD Information.
    def update_dvd(self, movie_name, dvd_data):
        name = input("Please Enter the Name of the DVD that you would like to Update:")
        with open(self.file_path, "r") as file:
            reader = csv.reader(file)
            rows = list(reader)
        for i, row in enumerate(rows):
            if row[0] == name:
                print("Please Enter the Updated DVDs Information:")
                row[0] = input("Please Enter the Name of the Movie:")
                row[1] = input("Please Enter the Name of the Stars:")
                row[2] = input("Please Enter the Name of the Producer:")
                row[3] = input("Please Enter the Name of the Director:")
                row[4] = input("Please Enter the Name of the Production Company:")
                row[5] = input("Please Enter the Number of Copies in the Store:")
                row[6] = input("Please Enter the Rental Fee of the DVD:")
                with open(self.file_path, "w", newline='') as file:
                    writer = csv.writer(file)
                    writer.writerows(rows)
                for dvd in self.dvds:
                    if dvd.movie_name == name:
                        dvd.movie_name = row[0]
                        dvd.stars = row[1]
                        dvd.producer = row[2]
                        dvd.director = row[3]
                        dvd.production_company = row[4]
                        dvd.copies = row[5]
                        dvd.rental_fee = row[6]
                        break
                print("DVD Information has been Updated Successfully!")
                return
        print(f"DVD '{name}' is Not Found.")




    # Remove a DVD Information.
    def remove_dvd(self, name):
        with open("dvds_list.csv", "r") as f:
            reader = csv.reader(f)
            header = next(reader)
            dvds = [row for row in reader if row[0] != name]

        with open("dvds_list.csv", "w", newline='') as f:
            writer = csv.writer(f)
            writer.writerow(header)
            writer.writerows(dvds)
        
        for dvd in self.dvds:
            if dvd.movie_name == name:
                self.dvds.remove(dvd)
                print(f"DVD '{name}' has been removed from the store.")
                break
        else:
            print(f"DVD '{name}' was not found in the store.")





    # Print a List of all the DVDs in the Store.
    def build_list(self):
            with open(self.file_path, 'r') as file:
                reader = csv.reader(file)
                self.header = next(reader)  # skip header row
                self.dvds = [DVD(*row) for row in reader]
            
            
    def update_dvd(self, movie_name, dvd_data):
            for dvd in self.dvds:
                if dvd.movie_name == movie_name:
                    dvd.movie_name = dvd_data[0]
                    dvd.stars = dvd_data[1]
                    dvd.producer= dvd_data[2]
                    dvd.director= dvd_data[3]
                    dvd.production_company= dvd_data[4]
                    dvd.copies= int(dvd_data[5])
                    dvd.rental_fee= float(dvd_data[6])
                    break

    def _merge_sort(self, dvds):
        if len(dvds) > 1:
            mid = len(dvds) // 2
            left_half = dvds[:mid]
            right_half = dvds[mid:]

            self._merge_sort(left_half)
            self._merge_sort(right_half)

            i = j = k = 0
            while i < len(left_half) and j < len(right_half):
                if left_half[i].movie_name < right_half[j].movie_name:
                    dvds[k] = left_half[i]
                    i += 1
                else:
                    dvds[k] = right_half[j]
                    j += 1
                k += 1

            while i < len(left_half):
                dvds[k] = left_half[i]
                i += 1
                k += 1

            while j < len(right_half):
                dvds[k] = right_half[j]
                j += 1
                k += 1

        return dvds

    def sort_dvds_by_name(self):
            self.dvds = self._merge_sort(self.dvds)

    def print_all_dvds(self):
        for dvd in self.dvds:
            print(dvd)

    def update_file_sorted(self):
        with open(self.file_path, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['Name', 'Stars', 'Producer', 'Director', 'Production Company', 'Copies', 'Rental Fee'])
            for dvd in self.dvds:
                writer.writerow([dvd.movie_name, dvd.stars, dvd.producer, dvd.director, dvd.production_company, dvd.copies, dvd.rental_fee])

    def update_sort_and_print_all_dvds(self):
        self.build_list()
        self.sort_dvds_by_name()
        self.print_all_dvds()
        self.update_file_sorted()



    # Show the Details of a Particular DVD.
    def show_dvd(self):
        name = input("Please Enter the Name of a DVD that you would to See:")
        for dvd in self.dvds:
            if dvd.movie_name == name:
                print(f"Movie Name: {dvd.movie_name}")
                print(f"Stars: {dvd.stars}")
                print(f"Producer: {dvd.producer}")
                print(f"Director: {dvd.director}")
                print(f"Production Company: {dvd.production_company}")
                print(f"Number of Copies in the Store: {dvd.copies}")
                print(f"Rental Fee: {dvd.rental_fee}")
                break
        else:
            with open(self.file_path, 'r') as file:
                reader = csv.reader(file)
                next(reader) 
                for row in reader:
                    if row[0] == name:
                        print(f"Movie Name: {row[0]}")
                        print(f"Stars: {row[1]}")
                        print(f"Producer: {row[2]}")
                        print(f"Director: {row[3]}")
                        print(f"Production Company: {row[4]}")
                        print(f"Number of Copies in the Store: {row[5]}")
                        print(f"Rental Fee: {row[6]}")
                        break
                else:
                    print(f"DVD '{name}' was not Found in the Store.")



    # Check Whether a Particular DVD is in the Store.
    def check_dvd(self):
        name = input("Please Enter the Name of the DVD that you would like to Check:")
        for dvd in self.dvds:
            if dvd.movie_name == name:
                if dvd.copies == '0':
                    print(f"Copy of DVD '{name}' is not Available in the Store.")
                else:
                    print(f"DVD '{name}' is in the Store.")
                return
        with open(self.file_path, 'r') as file:
            reader = csv.reader(file)
            next(reader) 
            for row in reader:
                if row[0] == name:
                    if row[5] == '0':
                        print(f"Copy of DVD '{name}' is not Available in the Store.")
                    else:
                        print(f"DVD '{name}' is in the Store.")
                    return
        print(f"DVD '{name}' is not Found in the Store.")



    # Maintain Customer Database.
    # Add a New Customer Information.
    def add_customer(self):
        new_customer = []
        new_customer.append(input("Please Enter Customer's Account Number:"))
        new_customer.append(input("Please Enter Customer's First Name:"))
        new_customer.append(input("Please Enter Customer's Last Name:"))
        new_customer.append(input("Please Enter Customer's Rented DVDs History:"))
        new_customer.append(input("Please Enter Customer's DVDs Rent Date:"))
        new_customer.append(input("Please Enter Customer's DVDs Due Date:"))
        new_customer.append(input("Please Enter Customer's DVDs Return Date:"))
        new_customer.append(input("Please Enter Customer's DVDs Late Fees if any:"))
        with open('customer_list.csv', 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(new_customer)
        customer_tree.add_customer(new_customer)
        print("Customer Information has been added Successfully!")



    # Edit a Customer Information.
    def edit_customer(self):
        customer_list = []
        with open("customer_list.csv", 'r') as file:
            reader = csv.reader(file)
            for row in reader:
                customer_list.append(row)

        account_number = input("Please Enter the Account Number of the Customer that you would like to Edit:")
        for index, customer in enumerate(customer_list):
            if customer[0] == account_number:
                customer[1] = input("Please Enter the First Name:")
                customer[2] = input("Please Enter the Last Name:")
                customer[3] = input("Please Enter the Rented DVDs History:")
                customer[4] = input("Please Enter the Rent Date:")
                customer[5] = input("Please Enter the Due Date:")
                customer[6] = input("Please Enter the Return Date:")
                customer[7] = input("Please Enter the Late Fees if any:")
                break
        
        with open("customer_list.csv", 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(customer_list)
            self.load_customer_list()
            
        print("Customer Information has been Edited Successfully!")


    
    # Update a Customer Information.
    def update_customer(self):
        customer_list = []
        with open("customer_list.csv", 'r') as file:
            reader = csv.reader(file)
            for row in reader:
                customer_list.append(row)

        account_number = input("Please Enter the Account Number of the Customer that you would like to Update:")
        found = False
        for i in range(len(customer_list)):
            if customer_list[i][0] == account_number:
                found = True
                customer_list[i][1] = input("Please Enter the First Name:")
                customer_list[i][2] = input("Please Enter the Last Name:")
                customer_list[i][3] = input("Please Enter the Rented DVDs History:")
                customer_list[i][4] = input("Please Enter the Rent Date:")
                customer_list[i][5] = input("Please Enter the Due Date:")
                customer_list[i][6] = input("Please Enter the Return Date:")
                customer_list[i][7] = input("Please Enter the Late Fees if any:")
                break

        if found:
            with open("customer_list.csv", 'w', newline='') as file:
                writer = csv.writer(file)
                writer.writerows(customer_list)
                self.load_customer_list()
            print("The Customer Information has been Updated Successfully!")
        else:
            print("The Customer with the given Account Number was not Found.")



    # Print a List of all of the DVDs Rented by Each Customer.
    def list_all_dvds(self):
        customers = []
        with open("customer_list.csv", "r") as file:
            reader = csv.reader(file)
            header = next(reader)
            for row in reader:
                customers.append(row)

        with open("customer_list.csv", "r") as file:
            reader = csv.reader(file)
            header = next(reader)
            rentals = {}
            for row in reader:
                account_number = row[0]
                if account_number not in rentals:
                    rentals[account_number] = []
                rentals[account_number].append(row)

        for customer in customers:
            account_number = customer[0]
            print(f"Account Number: {account_number}")
            print(f"{header[1]} - {customer[1]}")
            print(f"{header[2]} - {customer[2]}")
            print(f"{header[3]} - {customer[3]}")
            print(f"{header[4]} - {customer[4]}")
            print(f"{header[5]} - {customer[5]}")
            print(f"{header[6]} - {customer[6]}")
            print(f"{header[7]} - {customer[7]}")
            print()
            print("DVDs are Rented by:")
            if account_number in rentals:
                for rental in rentals[account_number]:
                    print(rental[1])
            print()




    # Rent a DVD, that is to check out a DVD.
    def rent_dvd(self, account_number):
        title = input("Please Enter the Name of the DVD that you would like to Rent:")

        # Check if the DVD is available in the store.
        with open("dvds_list.csv", "r") as file:
            reader = csv.reader(file)
            header = next(reader)
            rows = [row for row in reader]

        dvd_available = False
        dvd_index = -1
        for i, row in enumerate(rows):
            if row[0].lower() == title.lower():
                dvd_index = i
                if int(row[5]) > 0:
                    dvd_available = True
                    break

        if dvd_available:
            # Rent the DVD and deduct the number of copies from the dvds_list.csv file.
            rows[dvd_index][5] = str(int(row[5]) - 1)

            with open("dvds_list.csv", "w", newline="") as file:
                writer = csv.writer(file)
                writer.writerow(header)
                writer.writerows(rows)

            # Updating the Customer's Rented DVDs Information in the customer_list.csv File.
            with open("customer_list.csv", "r") as file:
                reader = csv.reader(file)
                header = next(reader)
                rows = [row for row in reader]

            for i, row in enumerate(rows):
                if int(row[0]) == account_number:
                    row[3] = title
                    row[3] = title
                    break

            with open("customer_list.csv", "w", newline="") as file:
                writer = csv.writer(file)
                writer.writerow(header)
                writer.writerows(rows)

            print("{} is now rented to your account.".format(title))
        else:
            print("Sorry, {} is not available in the store.".format(title))

    
   
    # Return a DVD or check in a DVD.
    def return_dvd(self, account_number):
        # Requesting for the Account Number, DVD Name and the Rent Date.
        dvd_name = input("Please Enter the DVD name:")
        rent_date = input("Please Enter the Rent Date (dd/mm/yyyy):")
        return_date = input("Please Enter the Return Date (dd/mm/yyyy):")
        due_date = input("Please Enter the Due Date (dd/mm/yyyy):")

        # Converting the Dates to a Datetime Objects.
        rent_date = datetime.strptime(rent_date, '%d/%m/%Y')
        return_date = datetime.strptime(return_date, '%d/%m/%Y')
        due_date = datetime.strptime(due_date, '%d/%m/%Y')

        # Calculating the Number of Days between the Rent and Return Date.
        number_of_days = (return_date - rent_date).days

        # Calculating the Number of Days Late.
        number_of_days_late = max(0, (return_date - due_date).days)

        # Constants for the rent per day and Late Fees.
        rent_per_day = 1.50
        late_fee = 1.00

        # Calculating the Total Charges.
        total_charge = (rent_per_day * number_of_days) + (number_of_days_late * late_fee)

        print(f"Total charge: ${total_charge:.2f}")
        confirm_payment = input("Would you like to proceed with the payment (yes/no)?")

        if confirm_payment.lower() == "yes":
            print("Payment Accepted!")

            # Updating the Customer's Rented DVDs Information in the customer_list.csv File.
            with open("customer_list.csv", "r") as file:
                reader = csv.reader(file)
                header = next(reader)
                rows = [row for row in reader]

            for i, row in enumerate(rows):
                if int(row[0]) == account_number:
                    row[3] = "Returned"
                    row[7] = "Paid"
                    break

            with open("customer_list.csv", "w", newline="") as file:
                writer = csv.writer(file)
                writer.writerow(header)
                writer.writerows(rows)

            # Deducting the Returned DVD from the dvds_list.csv File.
            with open("dvds_list.csv", "r") as file:
                reader = csv.reader(file)
                header = next(reader)
                rows = [row for row in reader]

            for i, row in enumerate(rows):
                if row[0] == dvd_name:
                    row[5] = str(int(row[5]) + 1)
                    break

            with open("dvds_list.csv", "w", newline="") as file:
                writer = csv.writer(file)
                writer.writerow(header)
                writer.writerows(rows)


    
store = DVDStore('dvds_list.csv')
store.load_dvd_list()

customer_tree = CustomerBTreeType('customer_list.csv')
customer_tree.build_tree()
customers = customer_tree.get_customers()

