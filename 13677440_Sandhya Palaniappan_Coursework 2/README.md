Data Structures & Algorithms - Python Programming Assignment:
This assignment focuses on the implementation of various data structures and algorithms using Python. It is divided into two main parts: practical exercises and a mini-project. The practical exercises include tasks on algorithms, search, sort, and recursion, while the mini-project involves developing a stock inventory application with features like adding, removing, displaying stock items, implementing search and sort algorithms, and additional creative functionalities. The assignment aims to provide hands-on experience in applying theoretical concepts to practical problems, enhancing the understanding of data structures and algorithmic thinking.


DVD Store Application System:
The DVD Store Application System is designed to manage the operations of a DVD store efficiently. It includes functionalities such as renting and returning DVDs, maintaining a list of DVDs owned by the store, checking the availability of DVDs, and managing customer information. The application uses a binary search tree for efficient customer data management and a linked list for handling the DVD inventory. The project demonstrates the application of object-oriented programming principles, data structures, and algorithms to solve complex problems in a real-world scenario. It includes comprehensive unit testing, integration testing, and system testing to ensure the reliability and correctness of the system.


Thank you so much for your kind support, everyone!