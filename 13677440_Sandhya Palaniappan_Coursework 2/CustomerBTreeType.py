

import csv
from Customer import Customer


# class Node
class Node:
    def __init__(self, data, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right


# class CustomerBTreeType
class CustomerBTreeType:
    def __init__(self, file_path):
        self.root = None
        self.file_path = file_path

    def initialize_customer_tree(self, customer_list):
        for customer in customer_list:
            print(customer)

    def add_customer(self, customer_data):
        if self.root is None:
            self.root = Node(customer_data)
        else:
            current_node = self.root
            while True:
                if int(customer_data[0]) < int(current_node.data[0]):
                    if current_node.left is None:
                        current_node.left = Node(customer_data)
                        break
                    current_node = current_node.left
                else:
                    if current_node.right is None:
                        current_node.right = Node(customer_data)
                        break
                    current_node = current_node.right


    def edit_customer(self, account_number, customer_data):
        account_number = int(account_number)
        current_node = self.root
        while current_node is not None:
            if account_number == int(current_node.data[0]):
                current_node.data = customer_data
                return True
            elif account_number < int(current_node.data[0]):
                current_node = current_node.left
            else:
                current_node = current_node.right
        return False
    
    
    def update_customer(self, account_number, customer_data):
        account_number = int(account_number)
        current_node = self.root
        while current_node is not None:
            if account_number == int(current_node.data[0]):
                current_node.data = customer_data
                self.update_file()
                return True
            elif account_number < int(current_node.data[0]):
                current_node = current_node.left
            else:
                current_node = current_node.right
        return False
                  

    def in_order_traversal(self, node, customers):
        if node is not None:
            self.in_order_traversal(node.left, customers)
            customers.append(node.data)
            self.in_order_traversal(node.right, customers)
        return customers


    def build_tree(self):
        customers = []
        with open(self.file_path, 'r') as file:
            reader = csv.reader(file)
            header = next(reader)
            for row in reader:
                customers.append([row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]])
        for customer in customers:
            self.add_customer(customer)


    def search_customer(self, account_number):
        current_node = self.root
        while current_node is not None:
            if int(account_number) == int(current_node.data[0]):
                return current_node.data
            elif int(account_number) < int(current_node.data[0]):
                current_node = current_node.left
            else:
                current_node = current_node.right
        return None


    def get_customers(self):
        customers = self.in_order_traversal(self.root, [])
        return [Customer(*customer_data) for customer_data in customers]


if __name__ == '__main__':
    customer_tree = CustomerBTreeType('customer_list.csv')
    customer_tree.build_tree()
    customers = customer_tree.get_customers()
    for customer in customers:
        print(customer.account_number, customer.first_name, customer.last_name, customer.rented_dvds, customer.rent_date, customer.due_date, customer.return_date, customer.late_fees)
