

import csv 
from Customer import Customer

class CustomerList:
    def __init__(self, file_path):
        self.file_path = file_path
        self.customers = []

    def load_customers(self):
        with open(self.file_path, 'r') as file:
            reader = csv.reader(file)
            self.header = next(reader)
            for row in reader:
                customer = Customer(*row)
                self.customers.append(customer)

    def print_customers(self):
        # Printing Header
        print(f"{self.header[0]:<20} {self.header[1]:<20} {self.header[2]:<20} {self.header[3]:<20} {self.header[4]:<20} {self.header[5]:<20} {self.header[6]:<20} {self.header[7]:<20}")
        
        # Printing the Customer Data
        for customer in self.customers:
            print(f"{customer.account_number:<20} {customer.first_name:<20} {customer.last_name:<20} {customer.rented_dvds:<20} {customer.rent_date:<20} {customer.return_date:<20} {customer.due_date:<20} {customer.late_fees:<20}")

if __name__ == '__main__':
    customer_list = CustomerList('customer_list.csv')
    customer_list.load_customers()
    customer_list.print_customers()





