
import csv
import re
from DVDStore import DVDStore
from CustomerBTreeType import CustomerBTreeType



# Created an Admin Login.
def admin_login():
    with open("admin_credentials.csv", "r") as file:
        reader = csv.reader(file)
        try:
            admin_username, admin_password = next(reader)
        except StopIteration:
            print("No admin credentials found in the database")
            login_menu()
            return False

    # Prompting the Admin to Enter their Username.
    entered_username = input("Username:")

    # To Check if the Entered Username matches the Admin Username.
    if entered_username != admin_username:
        print("Access Denied. Username is Incorrect. Please Try Again!")
        login_menu()
        return False

    # Prompting the User to Enter their Password.
    entered_password = input("Password:")

    # To Check if the Entered Password Matches the Admin Password.
    if entered_password != admin_password:
        print("Access Denied. Password is Incorrect. Please Try Again!")
        login_menu()
        return False

    # Display a Message provided if Both of the Username and Password are Correct.
    print("Access Granted. Hi Admin, Welcome to the DVD Store Application System!")
    admin_menu()
    return True


    

# Customer Login.
def customer_login():
    while True:
        username = input("Username:")
        password = input("Password:")
        
        with open("customer_login.csv", "r") as file:
            reader = csv.reader(file)
            for row in reader:
                if row[0] == username and row[1] == password:
                    print(f"Hi {username}, Welcome to the DVD Store Application System!")
                    customer_menu()
                    return True
            
        with open("registered_customer_login.csv", "r") as file:
            reader = csv.reader(file)
            for row in reader:
                if row[0] == username and row[1] == password:
                    print(f"Hi {username}, Welcome to the DVD Store Application System!")
                    customer_menu()
                    return True
        print("Username or Password is Incorrect. Please Try Again!")
        login_menu()
        return False



# To Check the Username and Password Strength when a New Potential Customer Registers for an Account at the DVD Store Application System.
def check_username_strength(username):
    if len(username) < 8:
        return False, "Username is too short, minimum 8 characters."
    if not re.search("[a-zA-Z0-9!@#$%^&*]", username):
        return False, "Username should contain at least one letter, one number or one special character."
    return True, "Username is strong."

def check_password_strength(password):
    if len(password) < 8:
        return False, "Password is too short, minimum 8 characters."
    if not re.search("[a-zA-Z0-9!@#$%^&*]", password):
        return False, "Password should contain at least one letter, one number or one special character."
    return True, "Password is strong."



# To Check if the New Potential Customer has entered an exisiting username.
def check_username_exists(username):
    with open("registered_customer_login.csv", "r") as file:
        reader = csv.reader(file)
        for row in reader:
            if row[0] == username:
                return True
    return False

# Registering a New Potential Customer.
def register_customer():
    while True:
        username = input("Username:")
        result, message = check_username_strength(username)
        if result:
            if not check_username_exists(username):
                break
            else:
                print("Username already exists, please choose a different username.")
        else:
            print(message)
    
    while True:
        password = input("Password:")
        result = check_password_strength(password)
        if result:
            break
        print("Password is too weak, minimum should have 8 characters with 1 uppercase, 1 lowercase, 1 number and 1 special character.")
        
    with open("registered_customer_login.csv", "a") as file:
        writer = csv.writer(file)
        writer.writerow([username, password])
    
    print("User Registered into the DVD Store Application System Successfully, You can Login as a Customer Now!")
    login_menu()
    return True


store = DVDStore('dvds_list.csv')
store.load_dvd_list()


customer_tree = CustomerBTreeType('customer_list.csv')
customer_tree.build_tree()
customers = customer_tree.get_customers()



# Main Menu.
def main_menu():
    print("Main Menu:")
    print("1. Login")
    print("2. Exit")
    choice = int(input("Please Enter your Choice: "))
    if choice == 1:
        login_menu()
    elif choice == 2:
         exit()
    else:
            print("Invalid Option, Please Try Again!")
            main_menu()    

# Login Menu.
def login_menu():
    print("Welcome to the DVD Store Login Menu")
    print("A. Login as Admin")
    print("C. Login as Customer")
    print("Y. Register as a New Customer")
    print("R. Return to Main Menu")
    choice = input("Please Enter your Choice:")

    if choice == 'A':
        admin_login()
    
    elif choice == 'C':
        customer_login()

    elif choice == 'Y':
        print("Would you like to register as a new customer? (yes/no)")
    answer = input().lower() or input().upper()
    if answer == "yes" or answer == "Yes":
        register_customer()
    elif answer == "no" or answer == "No":
        main_menu()

    elif choice == 'R':
        main_menu()

    else:
        print("Invalid Choice. Please Try Again!")
        login_menu()



# Admin Menu.
def admin_menu():
    print("Welcome to the Admin Menu!")
    print("1. Add a New DVD Information")
    print("2. Remove a DVD Information")
    print("3. Display all DVDs Information")
    print("4. Edit a DVD Information")
    print("5. Show the Details of a Particular DVD in the Store")
    print("6. Check whether a Particular DVD is in the Store")
    print("7. Add a New Customer Information")
    print("8. Edit a Customer Information")
    print("9. Update a Customer Information")
    print("10. Print a List of all of the DVDs Rented by Each Customer")
    print("11. Use Customer Binary Search Tree")
    print("12. Logout")
    choice = int(input("Please Enter your Choice:"))

    if choice == 1:
        store.add_dvd()

    elif choice == 2:
        name = input("Please Enter the Name of the DVD to be Removed:")
        store.remove_dvd(name)

    elif choice == 3:
        store.update_sort_and_print_all_dvds()

    elif choice == 4:
        store.edit_dvd()

    elif choice == 5:
        store.show_dvd()

    elif choice == 6:
        store.check_dvd()

    elif choice == 7:
        store.add_customer()

    elif choice == 8:
        store.edit_customer()

    elif choice == 9:
        store.update_customer()

    elif choice == 10:
        store.list_all_dvds()
    
    elif choice == 11:
            account_number = int(input("Please Enter the Account Number of the Customer that you would like to Search:"))
            customer = customer_tree.search_customer(account_number)
            if customer is not None:

                print("Account Number: ", customer[0])
                print("First Name: ", customer[1])
                print("Last Name: ", customer[2])
                print("Rented DVDs History: ", customer[3])
                print("Rent Date: ", customer[4])
                print("Due Date: ", customer[5])
                print("Return Date: ", customer[6])
                print("Late Fees: ", customer[7])

            else:
                print(f"Customer with the Account Number {account_number} was not found.")

            customer_tree.build_tree()
            customer_tree.get_customers()

    elif choice == 12:
        print(f"Thank you for visiting the DVD Store Application System, Admin!")
        main_menu()
    
    else: 
        print("Invalid Choice, Please Try Again!")

    admin_menu()


# Customer Menu
def customer_menu():
    print("Welcome to the Customer Menu!")
    print("1. Rent a DVD")
    print("2. Return a DVD")
    print("3. Display all DVDs Information")
    print("4. Show the Details of a Particular DVD in the Store")
    print("5. Check whether a Particular DVD is in the Store")
    print("6. Logout")
    choice = int(input("Please Enter your Choice:"))

    if choice == 1:
        account_number = int(input("Please Enter your Account Number:"))
        store.rent_dvd(account_number)

    elif choice == 2:
        account_number = int(input("Please Enter you Account Number:"))
        store.return_dvd(account_number)

    elif choice == 3:
        store.update_sort_and_print_all_dvds()

    elif choice == 4:
        store.show_dvd()

    elif choice == 5:
        store.check_dvd()

    elif choice == 6:
        print(f"Thank you for visiting the DVD Store Application System, See You Again!")
        main_menu()
    
    else: 
        print("Invalid Choice, Please Try Again!")

    customer_menu()

main_menu()

