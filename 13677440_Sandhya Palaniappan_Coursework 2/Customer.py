

# class Customer 
class Customer:
    def __init__(self, account_number, first_name, last_name, rented_dvds, rent_date, return_date, due_date, late_fees):
        self.account_number = account_number
        self.first_name = first_name
        self.last_name = last_name
        self.rented_dvds = rented_dvds
        self.rent_date = rent_date
        self.return_date = return_date
        self.due_date = due_date
        self.late_fees = late_fees
        
    def __str__(self):
        return f'{self.account_number},{self.first_name},{self.last_name},{self.rented_dvds},{self.rent_date},{self.return_date},{self.due_date},{self.late_fees}' 