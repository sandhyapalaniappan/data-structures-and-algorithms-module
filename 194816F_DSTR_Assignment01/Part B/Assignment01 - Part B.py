from itemList import item_description, item_selling_price, item_stock_level

flag = 0
total_cost = 0
cart = []

# Phase 01 - Stock Inventory (Basic Features)
groceryStore = {
    0: {'Item Description': 'Eggs', 'Item Stock Level': 70, 'Item Selling Price': 3.50, 'Item Expiration Date': '30 June 2020', 'Eligibility of Discount': ' Not Eligible'},
    1: {'Item Description': 'Bread', 'Item Stock Level': 80, 'Item Selling Price': 2.00, 'Item Expiration Date': '24 June 2020', 'Eligibility of Discount': 'Eligible'},
    2: {'Item Description': 'Milk', 'Item Stock Level': 100, 'Item Selling Price': 2.10, 'Item Expiration Date': '15 June 2020', 'Eligibility of Discount': 'Eligible'},
    3: {'Item Description': 'Cereals', 'Item Stock Level': 75, 'Item Selling Price': 5.40, 'Item Expiration Date': '14 September 2020', 'Eligibility of Discount': 'Eligible'},
    4: {'Item Description': 'Rice', 'Item Stock Level': 100, 'Item Selling Price': 6.20, 'Item Expiration Date': '16 October 2024', 'Eligibility of Discount': 'Not Eligible'},
    5: {'Item Description': 'Pasta', 'Item Stock Level': 150, 'Item Selling Price': 5.85, 'Item Expiration Date': '16 April 2022', 'Eligibility of Discount': 'Not Eligible'},
    6: {'Item Description': 'Ice Cream', 'Item Stock Level': 90, 'Item Selling Price': 10.90, 'Item Expiration Date': '15 September 2020', 'Eligibility of Discount': 'Not Eligible'},
    7: {'Item Description': 'Sausage', 'Item Stock Level': 85, 'Item Selling Price': 3.40, 'Item Expiration Date': '17 June 2020', 'Eligibility of Discount': 'Not Eligible'},
    8: {'Item Description': 'Chocolate Chip Cookies', 'Item Stock Level': 60, 'Item Selling Price': 5.90, 'Item Expiration Date': '25 July 2020', 'Eligibility of Discount': 'Eligible'},
    9: {'Item Description': 'Instant Coffee Mix', 'Item Stock Level': 65, 'Item Selling Price': 7.30, 'Item Expiration Date': '14 April 2023', 'Eligibility of Discount': 'Eligible'},
    10: {'Item Description': 'Harvest Barley Drink', 'Item Stock Level': 102, 'Item Selling Price': 6.60, 'Item Expiration Date': '16 June 2020', 'Eligibility of Discount': 'Eligible'},
    11: {'Item Description': 'Watermelon', 'Item Stock Level': 50, 'Item Selling Price': 6.65, 'Item Expiration Date': '16 June 2020', 'Eligibility of Discount': 'Not Eligible'},
    12: {'Item Description': 'Ferrero Rocher Chocolate', 'Item Stock Level': 95, 'Item Selling Price': 9.90, 'Item Expiration Date': '12 May 2021', 'Eligibility of Discount': 'Eligible'},
    13: {'Item Description': 'Nutella Hazelnut Spread', 'Item Stock Level': 100, 'Item Selling Price': 5.50, 'Item Expiration Date': '24 July 2020', 'Eligibility of Discount': 'Not Eligible'},
    14: {'Item Description': 'Sunflower Cooking Oil', 'Item Stock Level': 120, 'Item Selling Price': 7.20, 'Item Expiration Date': '16 November 2022', 'Eligibility of Discount': 'Eligible'},
    15: {'Item Description': 'Yakult Milk Bottle Drink - Apple', 'Item Stock Level': 66, 'Item Selling Price': 3.20, 'Item Expiration Date': '10 August 2020', 'Eligibility of Discount': 'Not Eligible'},
}

def find_two_numbers(SEQ, Z):
    """Use two-pointer technique to find two numbers that sum to Z in a sorted sequence of integers."""
    left, right = 0, len(SEQ) - 1
    while left < right:
        current_sum = SEQ[left] + SEQ[right]
        if current_sum == Z:
            return SEQ[left], SEQ[right], True
        elif current_sum < Z:
            left += 1
        else:
            right -= 1
    return None, None, False

def handle_find_two_numbers():
    """Handle finding two numbers that sum to Z based on user input."""
    # Prompt the user to enter a sorted sequence of integers. At least two numbers are needed for this process to work.
    SEQ = sorted([int(x) for x in input("Enter a sorted sequence of at least two integers, separated by spaces: ").split()])
    Z = int(input("Enter the value of Z: "))
    x, y, found = find_two_numbers(SEQ, Z)
    if found:
        print(f"X = {x}\nY = {y}\nTRUE (since X + Y = {Z})")
    else:
        print("X = not found\nY = not found\nFALSE (since there does not exist two integers X and Y in SEQ where X + Y = {Z})")

def menu():
    print('Welcome to MamaStore!')
    print('Press 1: To Add some Grocery Items.')
    print('Press 2: To Display the Grocery Items.')
    print('Press 3: To Remove the Grocery Items.')
    print('Press 4: To Use the Insertion Sort.')
    print('Press 5: To Use the Bubble Sort.')
    print('Press 6: To Use the Binary Search.')
    print('Press 7: To View the Total & Average Stock Level of the Grocery Store.')
    print('Press 8: To Purchase Grocery Items in the Stock Inventory.')
    print('Press 9: To Checkout Grocery Items in the Stock Inventory.')
    print('Press 10: To Print the Cart Items.')
    print('Press 11: To Inquire about an Grocery Store Item.')
    print('Press 12: For Help.')
    print('Press 13: To Find Two Numbers That Sum to a Given Value.')  # Phase 4 addition
    print('Press q: To Quit program.')
    return input('What would you like to do?')

# Phase 02 - Insertion Sort (Stock Inventory – Search and Sort Algorithms)
def insertionSort(theSeq, key):
    temp1 = [i for i in theSeq.values()]
    for i in range(0, len(temp1)):
        temp = temp1[i][key]
        j = i
        while j > 0 and temp < temp1[j - 1][key]:
            temp1[j][key] = temp1[j - 1][key]
            j = j - 1
        temp1[j][key] = temp
        s = {}
        for i in range(len(temp1)):
            s[i] = temp1[i]
        return s

# Sorting Menu
def sort_menu(second_list, sort_type):
    print('1. Sort by Item Description')
    print('2. Sort by Item Selling Price')
    print('Press b to go back.')

    # To Get the User Input
    user_input = input('Please Enter your Choice:')
    if user_input == '1':
        second_list = sort_type(groceryStore, 'Item Description')
        print(sorted(item_description.values()))
        print('Yes! Stock Inventory is sorted by Item Description!')
    elif user_input == '2':
        print(sorted(item_selling_price.values()))
        second_list = sort_type(groceryStore, 'Item Selling Price')
        print('Yes! Stock Inventory is sorted by Item Selling Price!')
    elif user_input == 'b':
        return second_list
    else:
        print('No! You have entered an invalid option!')
    # Return the updated list
    return second_list

# Phase 02 - Bubble Sort (Stock Inventory – Search and Sort Algorithms)
def bubble_sort(second_list, key):
    # Create Temp Copy of List
    temp = [i for i in second_list.values()]

    # To Obtain length of List
    y = len(temp)

    for i in range(y - 1, 0, -1):
        for j in range(i):
            if temp[j][key] > temp[j + 1][key]:
                temp[j], temp[j + 1] = temp[j + 1], temp[j]

    # To Return Updated List
    s = {}
    for i in range(len(temp)):
        s[i] = temp[i]
    return s

# Phase 02 - Binary Search (Stock Inventory – Search and Sort Algorithms)
founditem = ''
def binarysearch(dictionary, item):
    global founditem
    itemlist = []
    for item2 in dictionary:
        itemlist.append(item2)
    first = 0
    last = len(itemlist) - 1
    found = False

    while first <= last and not found:
        midpoint = (first + last) // 2
        if itemlist[midpoint] == item:
            founditem = dictionary.get(midpoint)
            found = True
        else:
            if item < itemlist[midpoint]:
                last = midpoint - 1
            else:
                first = midpoint + 1
    return founditem

# Phase 02 - Part E (Allow the User to Compute and Display the Total and Average Item Stock level for All the Items in the Stock Inventory)
def average(groceryStore):
    n = len(groceryStore)
    total = 0
    for i in range(0, n):
        total += groceryStore[i]["Item Stock Level"]
    average = total / n
    return average

def total(groceryStore):
    n = len(groceryStore)
    total = 0
    for i in range(0, n):
        total += groceryStore[i]["Item Stock Level"]
    return total

# Convert the dictionary to a sorted list of stock levels for the sequence
SEQ = sorted([(key, item['Item Stock Level']) for key, item in groceryStore.items()], key=lambda x: x[1])

# Main Loop
while True:
    run = menu()
    if run == '1':
        itemNumber = input('Please Enter the Item Number:')
        itemDescription = str(input('Which Item would you like to add to the Grocery Store?'))
        itemPrice = float(input('What is the selling price of the item?'))
        itemQuantity = int(input('Please Enter the Item Stock Level:'))
        itemExpiry = input('Please Enter the Item Expiry Date: ')
        itemDiscount = str(input('Please Enter the Eligibility of Discount (choose: eligible or not eligible): '))
        groceryStore[len(groceryStore)] = {'Item Description': itemDescription, 'Item Stock Level': itemQuantity, 'Item Selling Price': itemPrice, 'Item Expiration Date': itemExpiry, 'Eligibility of Discount': itemDiscount}
        print('Item Number:', itemNumber, 'Item Description:', itemDescription, 'Item Stock Level:', itemQuantity, 'Item Selling Price:', itemPrice, 'Item Expiration Date:', itemExpiry, 'Eligibility of Discount:', itemDiscount)

    # Phase 01 - Part B (To Display Items in the Stock Inventory)
    elif run == '2':
        for i in groceryStore:
            item = groceryStore[i]
            print(
                "Item No - %d Item Description - %s, Item Stock Level - %d, Item Selling Price - %.2f, Item Expiration Date - %s, Eligibility of Discount - %s." % (
                    i, item['Item Description'], item['Item Stock Level'], item['Item Selling Price'], item['Item Expiration Date'], item['Eligibility of Discount']))

    # Phase 01 - Part B (To Remove Items from the Stock Inventory)
    elif run == '3':
        remove = int(input('Please key in the item number that you would like to remove: '))
        del groceryStore[remove]

    # Phase 02 - Insertion Sort (Stock Inventory – Search and Sort Algorithms)
    elif run == '4':
        groceryStore = sort_menu(groceryStore, insertionSort)

    # Phase 02 - Bubble Sort (Stock Inventory – Search and Sort Algorithms)
    elif run == '5':
        groceryStore = sort_menu(groceryStore, bubble_sort)

    # Phase 02 - Binary Search (Stock Inventory – Search and Sort Algorithms)
    elif run == '6':
        itemNum = int(input('Enter the Item Number that you would like to search: '))
        print(binarysearch(groceryStore, itemNum))
        if itemNum not in groceryStore:
            print("The Item Number is Not Found!")

    # Phase 02 - Part E (Allow the User to Compute and Display the Total and Average Item Stock level for All the Items in the Stock Inventory)
    elif run == '7':
        print('The Total Stock Level for All the Items in the stock Inventory are:', total(groceryStore))
        print('The Average Stock Level for All the Items in the stock Inventory are:', round(average(groceryStore)))

    # Phase 03 - Stock Inventory (Additional Features)
    # To Purchase Grocery Items in the Stock Inventory.
    elif run == '8':
        p_no = int(input("Please Enter the Item Number: "))
        if (p_no in item_selling_price):
            if (flag == 1):
                flag = 0
            stock_current = item_stock_level.get(p_no)
            if (stock_current > 0):
                stock_current = item_stock_level.get(p_no)
                item_stock_level[p_no] = stock_current - 1
                item_price = item_selling_price.get(p_no)
                total_cost = total_cost + item_price
                print(item_description.get(p_no), "added to cart: ", "$", item_price)
                cart.append(p_no)  # Stores item in cart
            else:
                print("Sorry! We don't have that item in stock!")
        else:
            print("Sorry! We don't have such an item!")

    # To Checkout Grocery Items in the Stock Inventory.
    elif run == '9':
        print()
        print("You have bought the following parts: ", cart)
        print("Total:", "$", round(total_cost, 2))
        gst = round(0.12 * total_cost, 2)
        print("GST is 12%:", "$", gst)
        total = round(total_cost + gst, 2)
        print("After GST:", "$", total)
        total_cost = 0
        flag = 1
        print()
        print("You can still Purchase Grocery Store Items after checking out, Your cart has been reset!")
        print()

    # To Print the Cart Items.
    elif run == '10':
        print()
        print("You have bought the following Grocery Store Items (Item Number):", cart)
        print()

    # To Inquire about a particular Grocery Store Item.
    elif run == '11':
        print()
        p_no = int(input("Please Enter the Item Number: "))
        if (p_no in item_selling_price):
            print()
            print("Item number:", p_no, " Item Description:", item_description.get(p_no), "Item Selling Price:", item_selling_price.get(p_no), " Item Stock Level: ", item_stock_level.get(p_no))
            if (item_stock_level.get(p_no) < 3 and item_stock_level.get(p_no) != 0):
                print("Only ", item_stock_level.get(p_no), " remaining! Hurry!")
            print()
        else:
            print("Sorry we don't have such an item!")
            print()

    # Display all Commands
    elif run == '12':
        print('Help Centre')
        print('Press 1: To Add some Grocery Items.')
        print('Press 2: To Display the Grocery Items.')
        print('Press 3: To Remove the Grocery Items.')
        print('Press 4: To Use the Insertion Sort.')
        print('Press 5: To Use the Bubble Sort.')
        print('Press 6: To Use the Binary Search.')
        print('Press 7: To View the Total & Average Stock Level of the Grocery Store.')
        print('Press 8: To Purchase Grocery Items in the Stock Inventory.')
        print('Press 9: To Checkout Grocery Items in the Stock Inventory.')
        print('Press 10: To Print the Cart Items.')
        print('Press 11: To Inquire about an Grocery Store Item.')
        print('Press 12: For Help.')
        print('Press q: To Quit program.')

        # Phase 4 - Find two numbers that sum to Z
    elif run == '13':
        handle_find_two_numbers()

    elif run == 'q' or run == 'Q':
        confirm = input("Are you sure you want to quit? (y/n): ")
        if confirm.lower() == 'y':
            print("Thank you for using MamaStore, See you again!")
            break