def sequentialSearch(theValues, target):
    n = len(theValues)

    for i in range(n):
        if theValues[i] == target:
            return True

    return False

def sortedSequentialSearch(theValues, target):
    n = len(theValues)

    for i in range(n):
        if theValues[i] == target:
            return True

        elif theValues[i] > target:
            return False

listOfNum = [10, 7, 1, 3, -4, 2, 20]
print(sequentialSearch(listOfNum, 3))
print(sequentialSearch(listOfNum, 30))

sortedListOfNum = [-4, 1, 2, 3, 7, 10, 15, 20]
print(sortedSequentialSearch(sortedListOfNum, 10))
print(sortedSequentialSearch(sortedListOfNum, 30))