def binarySearch(theValues, target):
    low = 0
    high = len(theValues) - 1

    while low<=high:
         mid = (high + low) // 2

         if theValues[mid] == target:
            first_occurrence =  mid
            continuation = True
            while first_occurrence > 0 and continuation:
                 if theValues[first_occurrence - 1] == target:
                     first_occurrence -=1
                 else:
                     continuation = False
            return first_occurrence

         elif target < theValues[mid]:
             high = mid - 1

         else:
             low = mid + 1

    return - 1

sortedListOfNum = [1, 2, 7, 7, 18, 18, 25, 30, 33, 42, 56, 61, 78, 73, 88]
print(binarySearch(sortedListOfNum, 10))
print(binarySearch(sortedListOfNum, 73))
print(binarySearch(sortedListOfNum, 12))