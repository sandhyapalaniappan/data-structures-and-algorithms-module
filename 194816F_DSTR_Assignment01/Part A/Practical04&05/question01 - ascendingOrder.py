def selectionSort( theSeq ):
 n = len( theSeq )

 for i in range(n - 1):
  smallNdx = i

  for j in range(i+1, n):
      if theSeq[j] < theSeq[smallNdx]:
         smallNdx = j

  if smallNdx != i:
      tmp = theSeq[i]
      theSeq[i] = theSeq[smallNdx]
      theSeq[smallNdx] = tmp

list_of_numbers = [10, 51, 2, 18, 4, 31, 13, 5, 23, 64, 29]

print('Input List:', list_of_numbers)
selectionSort(list_of_numbers)
print('Sorted List:', list_of_numbers)
