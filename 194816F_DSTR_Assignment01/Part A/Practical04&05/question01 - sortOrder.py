def selectionSort( theSeq, sortOrder ):
 n = len( theSeq )

 for i in range(n - 1):
  smallNdx = i

  for j in range(i+1, n):
      if sortOrder == 'A':
          if theSeq[j] < theSeq[smallNdx]:
           smallNdx = j
      elif sortOrder == 'D':
          if theSeq[j] > theSeq[smallNdx]:
           smallNdx = j

  if smallNdx != i:
      tmp = theSeq[i]
      theSeq[i] = theSeq[smallNdx]
      theSeq[smallNdx] = tmp

list_of_numbers = [10, 51, 2, 18, 4, 31, 13, 5, 23, 64, 29]

print("Sorting in ascending Order:")
print('Input List:', list_of_numbers)
selectionSort(list_of_numbers, sortOrder='A')
print('Sorted List:', list_of_numbers)

print("Sorting in descending Order:")
print('Input List:', list_of_numbers)
selectionSort(list_of_numbers, sortOrder='D')
print('Sorted List:', list_of_numbers)