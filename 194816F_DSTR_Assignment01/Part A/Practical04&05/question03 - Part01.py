string = ['Bougainvillea', 'Orchids', 'Hibiscus', 'Frangipani', 'Honeysuckle']

def function(n): return n[1:5]

def num_list(list):
  return sorted(list, key=function)

print(num_list(['Bougainvillea', 'Orchids', 'Hibiscus', 'Frangipani', 'Honeysuckle']))