def insertionSort( theSeq ):
    n = len( theSeq )

    for i in range(1, n):
        value = theSeq[i]

        pos = i
        while pos > 0 and value < theSeq[pos - 1]:

            theSeq[pos] = theSeq[pos-1]
            pos -= 1

        theSeq[pos] = value

list_of_numbers = [10, 51, 2, 18, 4, 31, 13, 5, 23, 64, 29]
print('Input List:', list_of_numbers)
insertionSort(list_of_numbers)
print('Sorted List:', list_of_numbers)