def function(n): return n[-1]

def num_list(tuples):
  return sorted(tuples, key=function)

print(num_list([(1, 7), (1, 3), (3, 4, 5), (2, 2)]))