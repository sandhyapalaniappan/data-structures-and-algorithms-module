def recBinarySearch(target, theValues, first, last):
    if first > last:
        return False

    else:
        mid = (last+first) // 2

        if theValues[mid] == target:
            return True

        elif target < theValues[mid]:
            return recBinarySearch(target, theValues, first, mid-1)

        else:
            return recBinarySearch(target, theValues, mid+1, last)

sortedListofNum=[1,2,7,10,18,25,30,33,42,56,61,70,73,88]
print(recBinarySearch(10, sortedListofNum, 0, len(sortedListofNum)-1))
print(recBinarySearch(73, sortedListofNum, 0, len(sortedListofNum)-1))
print(recBinarySearch(12, sortedListofNum, 0, len(sortedListofNum)-1))