def rearrange_recursion(data, low, high):
    if low < high:
        if data[high] % 2 == 0:
            data[low], data[high] = data[high], data[low]

            rearrange_recursion(data, low+1, high)

        else:
            rearrange_recursion(data, low, high-1)


def organize(data):
    rearrange_recursion(data, 0, len(data)-1)

list_of_numbers = [1,2,3,4,5,6,7,8,9,10]

print('Original List:\t\t', list_of_numbers)
organize(list_of_numbers)
print('Re-arranged List:\t', list_of_numbers)